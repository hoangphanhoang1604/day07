<?php
session_start();
?>

<!DOCTYPE html>
<html lang="vn">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
input[type=text] {
    width: 182px; 
    height : 34px; 
    border: 2px solid #4f7ba3; 
    background-color: #e1eaf4;
}
</style>
<body>

    <form action="" method="POST" enctype="multipart/form-data" style=" width: 635px; margin: auto; margin-top: 50px;">

        <?php
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $phankhoa = isset($_POST['khoa']) ? $_POST['khoa']:"";
        }
        
        $liststudent = array(
            array("1", "Nguyễn Văn A", "Khoa học máy tính"),
            array("2", "Trần Thị B", "Khoa học máy tính"),
            array("3", "Nguyễn Hoàng C", "Khoa học vật liệu"),
            array("4", "Đinh Quang D", "Khoa học vật liệu")
        );

        $khoa = array(
            "" => "",
            "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu"
        );

        $show = array();
        if (!isset($_POST["khoa"]) or (isset($_POST["khoa"]) and $_POST["khoa"] == "")){
            $show = $liststudent;
        } else {
            $show = array();
            for ($i=0; $i<count($liststudent); $i++){
                if ($liststudent[$i][2] == $khoa[$_POST["khoa"]]){
                    array_push($show, array($liststudent[$i][0],$liststudent[$i][1],$liststudent[$i][2]));
                }
            }
        }
        ?>

        <?php 
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if(isset($_POST['add'])) {
                echo "<script> location.href='login.php'; </script>";
                exit;
            }
        }
        ?>

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial" style=" width: 120px; height: 40px; padding: 10px; ">
                Khoa</font>
            <div style='width: 330px; '>
                <select name="khoa" id="khoa"
                    style='width: 190px; height : 40px; border: 2px solid #4f7ba3; background-color: #e1eaf4;'
                    value="<?php echo $phankhoa;?>">
                    <?php 
                    $count = count($khoa);
                    foreach($khoa as $key => $value){
                        if ($key == $phankhoa){
                            echo '<option  selected="selected" value="',$key,'">',$value,'</option>';
                        } else {
                            echo '<option  value="',$key,'">',$value,'</option>';
                        }
                    }
                   ?>
                </select>
            </div>
        </div>

        <div class="container"
            style="display: flex;  justify-content: center;   align-items: center; margin-top: 20px;">
            <font face="Arial" style="width: 120px; height: 40px;  padding: 10px;">
                Từ khóa</font>
            <div style='width: 330px;'>
                <input type="text" name="key" id="key">
            </div>
        </div>

        <div style="display: flex;align-items: center; margin-top: 20px;">
            <button name="search"
                style="background-color: #4f81bd; border: 2px solid #385D8A; color: white;  width: 120px;  height: 40px; margin: auto; border-radius: 8px; margin-top: 10px;">
                Tìm kiếm </button>
        </div>

        <div style="display: flex; margin-top: 20px; justify-content: space-between;">
            <font face="Arial" style="height: 40px; padding: 10px;">
                Số sinh viên tìm thấy: <?php echo count($show); ?></font>

            <button name="add"
                style="background-color: #4f81bd; border: 2px solid #385D8A; color: white;  width: 80px;  height: 35px; border-radius: 8px; margin-right: 25px;">
                Thêm</button>
        </div>

        <table style="margin: auto; margin-top: 20px; ">
            <tr>
                <td style="width:50px">No</td>
                <td style="width:150px">Tên sinh viên</td>
                <td style="width:300px">Khoa</td>
                <td style="width:100px" colspan="2">Action</td>
            </tr>

            <?php  
            $lenData = count($show);
            for ($i=0; $i < $lenData; $i++){
                echo '<tr>
                <td>'.$show[$i][0].'</td>
                <td>'.$show[$i][1].'</td>
                <td>'.$show[$i][2].'</td>
                <td>
                    <button style="background-color: #92b1d6; border: 2px solid #395e8d; color: white;">
                        Xóa</button>
                </td>
                <td>
                    <button style="background-color: #92b1d6; border: 2px solid #395e8d; color: white;">
                        Sửa</button>
                </td>
            </tr>';
            }   
            ?>

        </table>





    </form>
</body>

</html>